// Create your HTML Page via DOM Methods here!

var body = document.body;

// TODO: Add a centered h1

header1 = document.createElement("h1");
header1.textContent = "Look Ma No HTML";
header1.setAttribute("style", "text-align: center;");
body.appendChild(header1);

// TODO: Add a centered h2

header2 = document.createElement("h2");
header2.textContent = "This webpage is written with JavaScript using the DOM to create elements that is translated into HTML";
header2.setAttribute("style", "text-align: center");
body.appendChild(header2);

// TODO: Add a centered image with a centered caption under it

img = document.createElement("img");
img.setAttribute("style", "height: 100px; width: 100px; margin: 0 auto; display: flex; justify-content: center;");
body.appendChild(img);

imgCaption = document.createElement("p");
imgCaption.textContent = "This is a placeholder for an image.";
imgCaption.setAttribute("style", "text-align: center;");
body.appendChild(imgCaption);

// TODO: Add a list of your favorite foods (or other favorites)

listHeader = document.createElement("h3");
listHeader.textContent = "My Favorite Movies";
body.appendChild(listHeader);

unorderedList = document.createElement("ul");
body.appendChild(unorderedList);

const movies = ["The Sunset Limited", "A Nightmare Before Christmas", "Moana"];

for (i = 0; i < movies.length; i++) {
    listItem = document.createElement("li");
    listItem.textContent = movies[i];
    unorderedList.appendChild(listItem);
}












